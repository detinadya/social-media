<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



#Route::get('/profile', function () {
#    return view('rotating_card.partials.rowcard');
#});

Auth::routes();

Route::resource('posts', 'PostController');
Route::resource('comments', 'CommentController');
Route::resource('users', 'UserController');


Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');

// DomPDF
Route::get('/test-dompdf', function () {
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/likepost/{id}/{user_id}', 'PostController@likepost');
Route::get('/likecomment/{id}/{user_id}', 'CommentController@likecomment');
Route::get('/follow/{id}/{user_id}', 'UserController@follow');
Route::get('/follow2/{id}/{user_id}', 'UserController@followtoprofile');
#userid di sini adalah pelaku liker,dan follower
Route::get('/mypost', 'PostController@mypost');

Route::get('/commentlangsung/{post_id}/', 'CommentController@commentlangsung');
Route::post('/storelangsung', 'CommentController@storelangsung');
Route::get('/profile', 'UserController@indexprofile');


#Route::get('/index2', function () {
#    return view('posts.index2');
#});

Route::get('/index2','PostController@daftarpost2');
