<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('posts.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //dd($request->all());
        #$request->validate([
        #    'title'=>'required|unique:posts',
        #    "body"=>'required'
        #]);
        //$query = DB::table('posts')->insert([
        //    "title"=>$request["title"],
        //    "body"=>$request["body"]
        //]);

        #$post = new Post;
        #$post->title = $request["title"];
        #$post->body = $request["body"];
        #$post->save();

        $post = Post::create([
            "title" => $request["title"],
            "content" => $request["content"],
            "picture" => 1, #$request["picture"],
            "quote" => $request["quote"],
            "user_id" => Auth::id(),
            "comment_id" => $request["comment_id"]
        ]);

        return redirect('/posts')->with('success', 'Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        $jumlahlike=DB::table('user_like_posts')->where('post_id',2)->count();
        return view('posts.show', compact('post','jumlahlike'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Post::where('id',$id)->update([
            "title" => $request["title"],
            "content" => $request["content"],
            "picture" => 1, #$request["picture"],
            "quote" => $request["quote"],
            "user_id" => Auth::id(), #ini sepertinya bisa dihilangkan
            "comment_id" => $request["comment_id"]
        ]);

        #dd($posts);
        return redirect('/posts')->with('success','Post Berhasil Disimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Post::destroy($id);
        return redirect('/posts')->with('success','Post Berhasil Dihapus');
    }


    public function mypost()
    {
        $user = Auth::user();
        $posts = $user->posts;
        return view('posts.index', compact('posts'));
    }

    public function likepost($id,$user_id){
        //dd($request->all());
        #$request->validate([
        #    'title'=>'required|unique:posts',
        #    "body"=>'required'
        #]);
        
        #$posts=DB::table('posts')
        #    ->where('id',$id)
        #    ->update([
        #        "title"=>$request["title"],
        #        "body"=>$request["body"]
        #    ]);
        //]);
        #$posts=DB::table('user_like_posts')
            
        #$update = user_like_posts::where('id',$id)->update([
        #            "title" => $request["title"],
        #            "body" => $request["body"]
        $query = DB::table('user_like_posts')->insert([
            "post_id"=>$id,
            "user_id"=>$user_id,
            "poin"=>1

        ]);
        return redirect('/posts')->with('success','berhasil like');
    }

    public function daftarpost2()
    {
        $posts = Post::all();
        return view('posts.index2', compact('posts'));
    }






}
