<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $comment = Comment::all();
        return view('comments.index', compact('comment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $comment = Comment::create([
            "comment_content" => $request["comment_content"],
            "post_id" => $request["post_id"],
            "user_id" => Auth::id()
        ]);
        return redirect('/comments')->with('success','Comment Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $comment = Comment::find($id);
        return view('comments.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $comment = Comment::find($id);
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = comment::where('id',$id)->update([
            "comment_content" => $request["comment_content"]
        ]);

        #dd($comments);
        return redirect('/comments')->with('success', 'Comment Berhasil Disimpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::destroy($id);
        return redirect('/comments')->with('success', 'Comment Berhasil Dihapus!');
    }

    public function mypost()
    {
        $user = Auth::user();
        $comments = $user->comments;
        return view('comments.index', compact('comments'));
    }

    public function likecomment($id,$user_id){
        //dd($request->all());
        #$request->validate([
        #    'title'=>'required|unique:posts',
        #    "body"=>'required'
        #]);
        
        #$posts=DB::table('posts')
        #    ->where('id',$id)
        #    ->update([
        #        "title"=>$request["title"],
        #        "body"=>$request["body"]
        #    ]);
        //]);
        #$posts=DB::table('user_like_posts')
            
        #$update = user_like_posts::where('id',$id)->update([
        #            "title" => $request["title"],
        #            "body" => $request["body"]
        $query = DB::table('user_like_comments')->insert([
            "comment_id"=>$id,
            "user_id"=>$user_id,
            "poin"=>1

        ]);
        return redirect('/comments')->with('success','berhasil like');
    }


    public function commentlangsung($post_id)
    {
        //
        return view('comments.createlangsung',compact('post_id'));
    }

    public function storelangsung(Request $request)
    {
        //
        $comment = Comment::create([
            "comment_content" => $request["comment_content"],
            "post_id" => $request["post_id"],
            "user_id" => Auth::id()
        ]);
        return redirect('/posts')->with('success','Comment Berhasil Diberikan pada Post!');
    }



}
