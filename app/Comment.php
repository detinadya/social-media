<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable =["comment_content","user_id","post_id"];
    public function author(){
        return $this->belongsTo('App\user','user_id');
    }
}
