@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center"><b>Dashboard</b></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p align="center">You Are Logged In!</p> 

                        <a class="nav-link btn btn-info" href="{{asset('/profile')}}">Go!</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
