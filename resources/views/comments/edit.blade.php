@extends('rotating_card.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">edit comment {{$comment->id}}</h3> --}}
              <h4 class="box-title">Edit Comment</h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/comments/{{$comment->id}}" method="POST">
              @csrf
              @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  {{-- <label for="comment_content">Comment Content</label> --}}
                  <input type="text" class="form-control" id="comment_content" name="comment_content" value="{{old('title',$comment->comment_content)}}" placeholder="Enter comment_content">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                {{-- <div class="form-group">
                  <label for="post_id">Post_id</label>
                  <input type="text" class="form-control" id="post_id" name="post_id" value="{{old('post_id',$comment->post_id)}}" placeholder="Enter Comment Content">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div> --}}
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
    </div>
</div>
@endsection