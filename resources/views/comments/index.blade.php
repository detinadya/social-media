@extends('rotating_card.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Comment</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(session('success'))
                <div class="alert alert-sucess">
                    {{session('success')}}
                </div>
             @endif
             <a button class="btn btn btn-primary mb-2" href="{{route('comments.create')}}">Create New Comment</a>
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>comment_content</th>
                  <th>penulis post</th>
                  {{-- <th>post_id</th>
                  <th>user_id</th> --}}
                  <th>jumlah like</th>
                  <th style="width: 40px">Label</th>
                </tr>
    
              </thead>
              <tbody>
                @forelse($comment as $key =>$comment)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$comment->comment_content}} </td>
                    <td> {{$comment->author->name}} </td>
                    {{-- <td> {{$comment->post_id}} </td>
                    <td> {{$comment->user_id}} </td> --}}
                    <td> {{DB::table('user_like_comments')->where('comment_id',$comment->id)->distinct('user_id')->count('user_id')}}</td>
                    {{--hitung jumlah like {{DB::table('user_like_comments')->where('comment_id',$comment->id)->count()}}--}}
                    {{--silahkan pilih hitung jumlah like uniq{{DB::table('user_like_comments')->where('comment_id',$comment->id)->distinct('user_id')->count('user_id')}}--}}
                    <td style="display: flex;">
                        {{-- <a href="/likecomment/{{$comment->id}}/{{Auth::id()}}" class="btn btn-success btn-sm">like</a> --}}
                        {{-- <a href="{{route('comments.show',['comment'=>$comment->id])}}" class="btn btn-primary btn-sm">show</a> --}}
                        {{-- <a href="/comments/{{$comment->id}}/edit" class="btn btn-info btn-sm">edit</a>
                        <form action="/comments/{{$comment->id}}" method="post">
                           @csrf
                           @method('DELETE')
                           <input type="submit" value="delete" class="btn btn-danger btn-sm"> 
                        </form> --}}
                        <div>   
                          <a href="/likecomment/{{$comment->id}}/{{Auth::id()}}" class="btn btn-success a-btn-slide-text">
                            <span class="fa fa-thumbs-o-up" aria-hidden="true"></span>
                            <span></span>            
                          </a>
                        </div>  
                        <div>   
                          <a href="/comments/{{$comment->id}}/edit" class="btn btn-info a-btn-slide-text">
                            <span class="fa fa-edit" aria-hidden="true"></span>
                            <span></span>            
                          </a>
                        </div>  
                        <div> 
                          <form action="/comments/{{$comment->id}}" method="post">
                            @csrf
                            @method('DELETE')
                              <button class="btn btn-danger btn-blok">
                              <i class="fa fa-trash"></i>
                              </button>
                          </form>
                        </div>
                    
                    </td>
                  </tr>
                @empty

                    <tr>
                    <td colspan="4" align="center">No Comment</td>
                    </tr>
                @endforelse

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
    </div>
@endsection