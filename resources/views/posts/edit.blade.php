@extends('rotating_card.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">edit post {{$post->id}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/posts/{{$post->id}}" method="POST">
              @csrf
              @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" value="{{old('title',$post->title)}}" placeholder="Enter Title">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="body">Body</label>
                  <input type="text" class="form-control" id="content" name="content" value="{{old('content',$post->content)}}" placeholder="Body">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="picture">Picture</label>
                  <input type="file" class="form-control" id="picture" name="picture" value="{{old('picture',$post->picture)}}" placeholder="Picture">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="quote">Quote</label>
                  <input type="text" class="form-control" id="quote" name="quote" value="{{old('quote',$post->quote)}}" placeholder="quote">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
    </div>
</div>
@endsection