@extends('rotating_card.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Posts</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(session('success'))
                <div class="alert alert-sucess">
                    {{session('success')}}
                </div>
             @endif
             <a button class="btn btn btn-primary mb-2" href="{{route('posts.create')}}">Create New Post</a>
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Picture</th>
                  <th>Quote</th>
                  <th>Jumlah Like</th>
                  <th>Penulis Post</th>
                  <th style="width: 40px">Label</th>
                </tr>
    
              </thead>
              <tbody>
                @forelse($posts as $key =>$post)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$post->title}} </td>
                    <td> {{$post->content}} </td>
                    <td> {{$post->quote}} </td>
                    
                    <td> {{DB::table('user_like_posts')->where('post_id',$post->id)->distinct('user_id')->count('user_id')}}</td>
                    {{--hitung jumlah like {{DB::table('user_like_posts')->where('post_id',$post->id)->count()}}--}}
                    {{--silahkan pilih hitung jumlah like uniq{{DB::table('user_like_posts')->where('post_id',$post->id)->distinct('user_id')->count('user_id')}}--}}
                    <td> {{$post->author->name}} </td>
                    <td style="display: flex;">
                        {{-- <a href="/likepost/{{$post->id}}/{{Auth::id()}}" class="btn btn-success btn-sm">like</a>
                        <a href="{{route('posts.show',['post'=>$post->id])}}" class="btn btn-primary btn-sm">show</a>
                        <a href="/posts/{{$post->id}}/edit" class="btn btn-info btn-sm">edit</a> --}}
                        {{-- <form action="/posts/{{$post->id}}" method="post">
                           @csrf
                           @method('DELETE')
                           <input type="submit" value="delete" class="btn btn-danger btn-sm"> 
                        </form> --}}
                      <div>   
                        <a href="/likepost/{{$post->id}}/{{Auth::id()}}" class="btn btn-success a-btn-slide-text">
                          <span class="fa fa-thumbs-o-up" aria-hidden="true"></span>
                          <span></span>            
                        </a>
                      </div>  
                      <div> 
                        <a href="{{route('posts.show',['post'=>$post->id])}}" class="btn btn-info a-btn-slide-text">
                          <span class="fa fa-eye" aria-hidden="true"></span>
                          <span></span>            
                        </a>
                      </div>  
                      <div>   
                        <a href="/posts/{{$post->id}}/edit" class="btn btn-warning a-btn-slide-text">
                          <span class="fa fa-edit" aria-hidden="true"></span>
                          <span></span>            
                        </a>
                      </div>  
                      <div> 
                        <form action="/posts/{{$post->id}}" method="post">
                          @csrf
                          @method('DELETE')
                            <button class="btn btn-danger btn-blok">
                            <i class="fa fa-trash"></i>
                            </button>
                        </form>
                      </div>
                      <div>   
                        <a href="/commentlangsung/{{$post->id}}/" class="btn btn-warning a-btn-slide-text">
                          <span class="fa fa-edit" aria-hidden="true">beri comment</span>
                          <span></span>            
                        </a>
                      </div> 


                    </td>
                  </tr>
                  @empty
                      <tr>
                        <td colspan="6" align="center">No Post </td>
                      </tr> 
                  @endforelse

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
    </div>
@endsection