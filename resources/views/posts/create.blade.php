@extends('rotating_card.master')
@push('script-head')
<script src="/path-to-your-tinymce/tinymce.min.js"></script>

@endpush
@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create New Post</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/posts" method="POST">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" value="{{old('title','')}}" placeholder="Enter Title">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="body">Content</label>
                  <input type="text" class="form-control" id="content" name="content" value="{{old('content','')}}" placeholder="Body">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <!-- input type utk upload berkas = FILE -->
                <div class="form-group">
                  <label for="picture">Picture</label>
                  <input type="file" class="form-control" id="picture" name="picture" value="{{old('picture','')}}" placeholder="Picture">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="quote">Quote</label>
                  {{-- <input type="text" class="form-control" id="quote" name="quote" value="{{old('quote','')}}" placeholder="quote"> --}}
                  {{-- laravel file manager --}}
                  <textarea name="quote" class="form-control my-editor">{!! old('quote', $quote ?? '') !!}</textarea>
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
    </div>
</div>
@endsection

@push('scripts')
  <script>
    var editor_config = {
      path_absolute : "/",
      selector: 'textarea.my-editor',
      relative_urls: false,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table directionality",
        "emoticons template paste textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      file_picker_callback : function(callback, value, meta) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
        if (meta.filetype == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.openUrl({
          url : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no",
          onMessage: (api, message) => {
            callback(message.content);
          }
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endpush