@extends('rotating_card.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User {{$user->name}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/users/{{$user->id}}" method="POST">
              @csrf
              @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{old('name',$user->name)}}" placeholder="Enter Title">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" value="{{old('email',$user->email)}}" placeholder="Enter Title">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="text" class="form-control" id="password" name="password" value="{{old('password',$user->password)}}" placeholder="Enter Title">
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="fullname">Fullname</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" value="{{old('fullname',$user->fullname)}}" placeholder="Body">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" id="address" name="address" value="{{old('address',$user->address)}}" placeholder="address">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="placeofbirth">Place of Birth</label>
                  <input type="text" class="form-control" id="placeofbirth" name="placeofbirth" value="{{old('placeofbirth',$user->placeofbirth)}}" placeholder="placeofbirth">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="dateofbirth">Date of Birth</label>
                  <input type="text" class="form-control" id="dateofbirth" name="dateofbirth" value="{{old('dateofbirth',$user->dateofbirth)}}" placeholder="dateofbirth">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="gender">Gender</label>
                  <input type="text" class="form-control" id="gender" name="gender" value="{{old('gender',$user->gender)}}" placeholder="gender">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
    </div>
</div>
@endsection