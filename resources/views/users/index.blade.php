@extends('rotating_card.master')

@section('content')

    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(session('success'))
                <div class="alert alert-sucess">
                    {{session('success')}}
                </div>
             @endif
            
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Jumlah Follower</th>
                  <th style="width: 40px">Label</th>
                </tr>
    
              </thead>
              <tbody>
                @forelse($users as $key =>$user)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$user->name}} </td>
                    <td> {{$user->email}} </td>
                    <td> {{DB::table('user_follow_users')->where('followed_id',$user->id)->distinct('follower_id')->count('follower_id')}}</td>
                    {{--hitung jumlah like {{DB::table('user_follow_users')->where('followed_id',$user->id)->count()}}--}}
                    {{--silahkan pilih hitung jumlah like uniq{{DB::table('user_follow_users')->where('followed_id',$user->id)->distinct('follower_id')->count('follower_id')}}--}}
                    <td style="display: flex;">
                        {{-- <form action="/users/{{$user->id}}" method="post">
                           @csrf
                           @method('DELETE')
                           <input type="submit" value="delete" class="btn btn-danger btn-sm"> 
                        </form> --}}
                      <div> 
                        <form action="/users/{{$user->id}}" method="post">
                          @csrf
                          @method('DELETE')
                             <button class="btn btn-danger btn-blok">
                             <i class="fa fa-trash"></i>
                             </button>
                         </form>
                       </div>
                    
                    </td>
                  </tr>
                @empty

                    <tr>
                    <p>No Users</p>
                    </tr>
                @endforelse

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
    </div>
@endsection